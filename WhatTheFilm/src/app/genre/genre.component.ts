import { Component, OnInit, Inject }            from '@angular/core';
import { MAT_DIALOG_DATA }                      from '@angular/material';
import { MatDialog }                            from '@angular/material';

@Component({
  selector: 'ns-genre',
  templateUrl: './genre.component.html',
  styleUrls: ['./genre.component.css'],
  moduleId: module.id,
})
export class GenreComponent implements OnInit {

  genre = {
    id : null,
    name : null
  }
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {

  }

}
