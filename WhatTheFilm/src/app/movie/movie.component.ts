import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ns-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.css'],
  moduleId: module.id,
})
export class MovieComponent implements OnInit {

  movie = {
    adult : false,
    backdrop_path : null,
    genre_ids: [],
    id: null,
    original_language: null,
    original_title: null,
    overview: null,
    poster_path: null,
    release_date: null,
    title: null,
    video: null,
    vote_average: null,
    vote_count: null
  }
  constructor() { }

  ngOnInit() {
  }

}
