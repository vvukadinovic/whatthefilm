import { Component, OnInit } from "@angular/core";
import { isAndroid } from "tns-core-modules/platform";
import 'firebase/firestore';

//import { firestore } from 'nativescript-plugin-firebase';

const firebase = require("nativescript-plugin-firebase");



@Component({
    selector: "ns-app",
    moduleId: module.id,
    templateUrl: "app.component.html",
    styleUrls: ["./app.component.scss"]
})


export class AppComponent implements OnInit {

    constructor() {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        firebase.init({
            // Optionally pass in properties for database, authentication and cloud messaging,
            // see their respective docs.
          }).then(
              function () {
                console.log("firebase.init done");
              },
              function (error) {
                console.log("firebase.init error: " + error);
              }
          );
          const genresCollection = firebase.firestore.collection("genres");
          genresCollection.get({ source: "server" }).then(querySnapshot => {
            querySnapshot.forEach(doc => {
              console.log(`${doc.id} => ${JSON.stringify(doc.data())}`);
            });
          });
        
    }

    getIconSource(icon: string): string {
        const iconPrefix = isAndroid ? "res://" : "res://tabIcons/";

        return iconPrefix + icon;
    }
}
